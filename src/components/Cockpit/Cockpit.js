
import React from 'react'
import classes from './Cockpit.css';
import Aux from '../../hoc/Auxiliary'

const Cockpit = props => {
    let btnClass = classes.Button
    let assignedClasses = []

    if (props.peopleNum < 3) {
      assignedClasses.push(classes.red)
    }
    if (props.peopleNum < 2) {
      assignedClasses.push(classes.bold)
    }
    if (props.peopleNum < 1) {
      assignedClasses = []
    }

    if (props.showPeople) {
        btnClass = [classes.Button, classes.Red].join(' ')
    }

    return(
        <Aux>
            <div className={classes.CockpitIntro}>
                <p className={assignedClasses.join(' ')}>
                To get started, edit <code>src/App.js</code> and save to reload.
                </p>
            </div>
            <button 
                className={btnClass}
                onClick={() => {props.switch('Pancracio')}}
                key = {'1'}>
                Switch Name
            </button>
            <button className={btnClass} onClick={props.show} key ='2'> Toogle People </button>
        </Aux>
    )
}

export default Cockpit
import React, {Component} from 'react';
import people from './Person.css'
import propTypes from 'prop-types'

class Person extends Component {
    constructor(props) {
        super(props)
        this.inputElement = React.createRef()
    }

    componentDidMount() {
        if (this.props.position === 0) {
            this.inputElement.current.focus()
        }
    }

    render() {
        return(
            <div className={people.person}>
                <p onClick={this.props.click}>My name is {this.props.name}, I have {Math.floor(Math.random() *30)} legs. </p>
                <p>Should I have {Math.round(Math.random() * 5, 2)} and {this.props.age} years? || {this.props.children}</p>
                <input 
                    ref={this.inputElement}
                    type="text"
                    onChange={this.props.value}
                    value={this.props.name}/>
            </div>
        ) 
    }
}

Person.propTypes = {
    clic: propTypes.func,
    name: propTypes.string,
    age: propTypes.number,
    onChange: propTypes.func
}

export default Person
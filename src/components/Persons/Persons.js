import React, {Component} from 'react'
import PersonImported from './Person/Person'

class Persons extends Component {
  render () {
    return this.props.people.map((person, index) => {
      return <PersonImported 
        position = {index}
        click={() => this.props.click(index)}
        name={person.name} 
        age={person.age}
        key={person.id}
        value={(event) => this.props.change(event, person.id)}
        />
    })
  }
} 

export default Persons
import React, { Component } from 'react';
import Persons from '../components/Persons/Persons';
import Cockpit from '../components/Cockpit/Cockpit'
import classes from './App.css'
import logo from '../logo.svg';


class App extends Component {
  state = {
    people: [
      {id: 11, name: 'Juan', age: 12},
      {id: 22, name: 'Diego', age: 23},
      {id: 33, name: 'Pedro', age: 54}
    ], 
    showPeople: false, 
    toogleClicked: 0
  }

  switchEventHandler = newName => {
    // DON'T DO THIS... this.state.people[0].name = 'Alberto'
    this.setState( {
      people: [
        {id: 12, name: newName, age: 12},
        {id: 62, name: 'Diego', age: 29},
        {id: 42, name: 'Pedro', age: 54}
      ]
    })
  }

  deleteEventHandler = personIndex => {
    // A copy of people is created to not get error modifing the original state
    const persons = [...this.state.people];
    persons.splice(personIndex, 1);
    this.setState({people: persons})
  }

  inputEventHolder = (event, personId) => {
    const personIndex = this.state.people.findIndex( per => {
      return per.id === personId
    })

    const person = {
      ...this.state.people[personIndex]
    }

    person.name = event.target.value

    const persons = [...this.state.people]
    persons[personIndex] = person
      
    this.setState({
      people: persons
    })
  }

  showEventHandler = () => {
    const currentState = this.state.showPeople;
    this.setState( (prevState, props) => {
      return {
        showPeople: !currentState,
        toogleClicked: prevState.toogleClicked + 1
      }
    });
  }

  render() {
    let gente = null
    

    if (this.state.showPeople) {
      gente = <Persons 
        people = {this.state.people}
        click = {this.deleteEventHandler}
        change = {this.inputEventHolder} />
    } 

    return (
      <div className={classes.App}>
        <header className={classes.AppHeader}>
            <img src={logo} className={classes.AppLogo} alt="logo" />
            <h1 className={classes.AppTitle}>Welcome to React</h1>
        </header>
        <Cockpit 
          peopleNum = {this.state.people.length}
          showPeople = {this.state.showPeople}
          switch = {this.switchEventHandler}
          show = {this.showEventHandler}
        />
        {gente}
      </div>
    );
  }
}

export default App;

/* ESTE ES EL APPROACH SI QUISIERA HACER LA VALIDACIÓN DEL CONTENIDO DENTRO DEL RETURN
  {
  this.state.showPeople === true ? 
    <div>
      <PersonImported 
        name={this.state.people[0].name} 
        age={this.state.people[0].age}
        />
      <PersonImported 
        name={this.state.people[1].name} 
        age={this.state.people[1].age}
        click={this.switchEventHandler.bind(this, 'ChangedNameWithClickIn2dnName')}
        value={this.inputEventHolder}
      > and I'm the best</PersonImported>
      <PersonImported 
        name={this.state.people[2].name} 
        age={this.state.people[2].age}
      />
    </div> : null
} */

/* Lifecycle: Todas deben ser definidas antes del render() así se ejecuten después
contructor()
componentWillMount() // Try to avoid it
componentWillReceiveProps(nextProps) // Try to avoid it
shouldComponentUpdate(nextProps, nextState) This methods returns true or false
componentWillUpdate() // Try to avoid it
render()
getSnapshotBeforeUpdate() // To load the previous scroll position
componentDidMount() 
componentDidUpdate()
--**--**--
static getDerivedStateFromProps(nextProps, prevState)
*/
